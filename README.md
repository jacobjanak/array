## Running the application:

Visit this url: https://jacobjanak.gitlab.io/array/

or

Clone the repository and then open index.html with your web browser.

##### Notes:
-I assumed the JSON data will always be in chronological order.
-I assumed the "Lock" button isn't meant to do anything because it doesn't in the demo.
-I didn't think there was any way to correctly implement the time zone so I defaulted it to GMT +5.
-I added a "show less" button. It made sense to do so, since there's a "show all" button.

##### Future improvements:
-I'm realizing now that I was supposed to copy ALL of the html into the component, not just the html code that belonged to the main widget. But I think it's okay, since it would only take a simple copy+paste to fix that and the rest of the html code wasn't relevant to the tasks.
-For better clarity and efficiency, the main component should be split into more components. For example, I think the list of locks/unlocks should be its own component.
-I am re-rendering way too much static code that won't ever change. If I broke this code up into more modular components, that wouldn't be an issue. The efficiency can be improved a lot.
-I think "show all" is potentially risky. What if the user's history is 1000+ entries long? The webpage may become unusable and, if their connection is bad, it may not load entirely. I think it's safer to have "show more", which can be clicked as many times as is necessary.
-Some changes could be made to better accomodate screen readers.
